# Snap Task  

## Pre define information

* I asume we have 2 bridge 1 private priv-br and 1 public bridge witch connect to network.
* I create private repository **[(Sonatype-NEXUS)](https://www.sonatype.com/nexus/repository-pro)** for Centos image and package manager witch cached all requred files.
* I create a simple file service for kick start files witch server all kickstart files in network.

## Kick Start

for kick start i used script blow

```
#version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512
# Use graphical install
#graphical
text
# Run the Setup Agent on first boot
firstboot --enable
ignoredisk --only-use=vda
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8

# Network information
#network  --bootproto=dhcp --device=eth0 --ipv6=auto --activate
network  --bootproto=static --ip=192.168.200.186 --device eth0 --netmask=255.255.255.0 --gateway=192.168.200.1 --nameserver=8.8.8.8 
network  --bootproto=static --ip=192.168.100.10 --device eth1 --netmask=255.255.255.0 
network  --hostname=tetstmachine.localdomain

# Use network installation
url --url="http://192.168.200.1:8081/repository/centos/7/os/x86_64/"
# Root password
rootpw --iscrypted $6$hDRPecUcLuuFn8nM$kS6hMRt6Na43hTK0o5pjPSxoZwdqVDlVvYXLFuHxuwhdNQwsFgCYgqwGwnVeMB.ckhmaiTUrkjlgFVQw0nMoo.
# System services
services --enabled="chronyd"
reboot
# System timezone
timezone Asia/Tehran --isUtc
user --groups=wheel --name=ansible --password=$6$Nbk9b/NYfKJ1zjbJ$rMcq/I/O9X9uNu28mjtst.FfCvxT0OS7.HBf1Uv38/kXIs9/COCTzJWDGQAYCmdi85rCwZYwh.JeFQPP/xP8l. --iscrypted --gecos="ansible"

sshkey --username=ansible "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCWaJZws7erKE54Of4DKQ0LcmsaSDRIEjjRCSgNGVcFCkIn0mHREKmclaIVtV8Z2XcfiYGA8hk6kxg823r7JLf9e5u4l12bPlb/1mXv/Nhwrmc4AQXRajMo1gk9Un4EhauVyaEMFXNZcqfGcDTfDEMZs/6o2Wd/we7U0aKnhFOFC9LOfODz8mcyxH2Q/rkV5Ecif1+dro3blLDGo2Uk/ct7XjMiutWDw1u9o4QPSc1yPdhb81mJ2dqRb0OhURYORg/AMLToLGlu+muo9CGaxmLdsGvkIlqB/QVOuttkoC4xnHLwJSAzkeaIbdXO5s1Ltk1J739HA3ZtMjo2krI9iR+oOVFAMDFKTJfGdM06maRI96j5ZRmueIjffayTKsUCJac/g3maLXXFnegPxGFdW0XWmCQ+6cTzxD25qNJc7fQwVPnTokySvJPltHxSHDz0jr7EM7TLxg8lswM60b2l9b5C1G9OgvLeZKeruHj4fGBIEgSZHUlDsgtg4N6ABPR9nYZtblN5Rk3fBTmBbhUcA+SNbGd5e/tDG0jMiadjL7Tb5WZfIfxBEc6nZeprhcRakL4zcRf9CsxNEx0ycYq6cjdBqi7BjG0IKfXLvc3p8p+o8zIfyvr8OxAoNES7kfFSCwuegQcoFtk7OxDLdcJHV3+DWABsIQugrebfCUVrkH2uxQ== id_rsa.old"

# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=vda
# Partition clearing information
clearpart --none --initlabel
# Disk partitioning information
part swap --fstype="swap" --ondisk=vda --size=4096
part / --fstype="xfs" --ondisk=vda --size=3072
part /boot --fstype="xfs" --ondisk=vda --size=2024
part /tmp --fstype="xfs" --ondisk=vda --size=4096
part /var --fstype="xfs" --ondisk=vda --size=5120

%packages
@^minimal
@core
chrony
kexec-tools
sudo

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%post
echo "ansible        ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers.d/ansible
# disable ping by kernel
echo "net.ipv4.icmp_echo_ignore_al=1" >> /etc/systctl.conf
%end

%anaconda
pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
%end
```

## install centos via virtsh-install

> virt-install  --connect=qemu:///system  -n  centos -r 2048 --vcpu 2 -v --network=bridge:virbr2  --network=bridge:PrivBr --disk size=18 -d --location http://192.168.200.1:8081/repository/centos/7/os/x86_64/   --name=centos7  -x "ks=http://192.168.200.1:8000/ks-minimal.cfg" -x "console=ttyS0" --cpu host  --graphic none
